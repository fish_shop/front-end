import React from 'react';
import {hydrate} from 'react-dom';
import {Provider} from 'react-redux';
import {HelmetProvider} from 'react-helmet-async';
import {RouterProvider} from 'react-router5'
import {useSSR} from 'react-i18next';
import App from './containers/App';
import routes from './routes';
import configureStore from './configureStore';
import createRouter, {onRouteActivateMiddleware} from './createRouter'

import './i18n';

const router = createRouter();
const store = configureStore(window.__PRELOADED_STATE__, router, false);

router.setDependency('store', store);
router.useMiddleware(onRouteActivateMiddleware(routes));

const RootComponent = () => {
    useSSR(window.initialI18nStore, window.initialLanguage);
    return (
        <Provider store={store}>
            <HelmetProvider>
                <RouterProvider router={router}>
                    <App/>
                </RouterProvider>
            </HelmetProvider>
        </Provider>
    )
};

router.start(
    window.__ROUTER_STATE__,
    (error, state) => {
        if (error) {
            console.error('router error', error, state);
        }

        hydrate(
            <RootComponent/>,
            document.getElementById('root')
        );

        if (module.hot) {
            module.hot.accept();
        }
    },
);

// Delete it once we have it stored in a variable
delete window.__PRELOADED_STATE__;
delete window.__ROUTER_STATE__;
