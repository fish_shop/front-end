import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet-async';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';

import Home from '../pages/Home';
import CatalogList from '../pages/Catalog/List';
import CatalogSingle from '../pages/Catalog/Single';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Sidebar from "../components/Sidebar";
import BlogList from '../components/Blog/List';
import BlogSingle from '../components/Blog/Single';
import Contacts from '../pages/Contacts'
import {fetchCategories} from '../thunks/Catalog';
import {fetchDiscountBySlug, fetchDiscountList} from "../thunks/Discounts";
import {fetchRecipeBySlug, fetchRecipesList} from "../thunks/Recipes";


import '../assets/scss/theme.scss';


class App extends Component {
    render() {
        let page = null;
        if (this.props.route) {
            switch (this.props.route.name) {
                case 'home':
                    page = <Home/>;
                    break;
                case "catalog":
                    page = <CatalogList />;
                    break;
                case 'catalog.category':
                    page = <CatalogList />;
                    break;
                case 'catalog.product':
                    page = <CatalogSingle />;
                    break;
                case 'discounts':
                    page = <BlogList
                        h1="Акции и скидки"
                        routeName="discounts"
                        listItems={this.props.discountList}
                        fetchList={this.props.fetchDiscountList}
                    />;
                    break;
                case 'discounts.single':
                    page = <BlogSingle
                        item={this.props.discountSingle}
                        fetchBySlug={this.props.fetchDiscountBySlug}
                    />;
                    break;
                case 'recipes':
                    page = <BlogList
                        h1="Рецепты"
                        routeName="recipes"
                        listItems={this.props.recipesList}
                        fetchList={this.props.fetchRecipesList}
                    />;
                    break;
                case 'recipes.single':
                    page = <BlogSingle
                        item={this.props.recipeSingle}
                        fetchBySlug={this.props.fetchRecipeBySlug}
                    />;
                    break;
                case 'contacts':
                    page = <Contacts/>;
                    break;
                default:
                    page = <h1>404</h1>;
            }
        }

        let header_categories = this.props.categories && this.props.categories.filter(category => category.is_header === true);

		return (
			<Fragment>
				<Helmet>
					<html lang={this.props.t('locale')} />

                    <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                    <meta charSet="utf-8"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>

					<title>{this.props.t('pages.' + this.props.route.name + '.h1')}</title>
					<meta property="og:title" content={this.props.t(this.props.route.name)} />
				</Helmet>

                <div id="outer-container">
                    <Sidebar burgerButtonClassName={ "sidebar-button" } elements={this.props.categories}/>

                    <main id="page-wrap">
                        <Header/>

                        <div id="tt-pageContent">
                            {page}
                        </div>

                        <Footer categories={this.props.categories}/>
                    </main>
                </div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
    route: state.router.route,
    categories: state.Catalog.categories,

    discountList: state.Discounts.list,
    discountSingle: state.Discounts.single,

    recipesList: state.Recipes.list,
    recipeSingle: state.Recipes.single,
});

const mapDispatchToProps = dispatch => ({
    fetchCategories: () => dispatch(fetchCategories()),
    fetchDiscountList: () => dispatch(fetchDiscountList()),
    fetchDiscountBySlug: (slug) => dispatch(fetchDiscountBySlug(slug)),
    fetchRecipesList: () => dispatch(fetchRecipesList()),
    fetchRecipeBySlug: (slug) => dispatch(fetchRecipeBySlug(slug)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(App));
