import React from 'react';
import { Link } from 'react-router5';
import {withTranslation} from "react-i18next";

import defaultImage from '../assets/images/loader-03.svg';
const badgeColor = 'green';

const ImageBoxBlock = ({ slug, images, example_is_new, t }) => {
    let image = defaultImage;
    if (images && images[0]) {
        image = images[0]["original"];
    }

    return (
        <div className="tt-image-box">
            <Link routeName={"/product/" + slug}>
                <span className="tt-img"><img src={image} alt=""/></span>
                <span className="tt-img-roll-over"><img src={image} alt=""/></span>
                { example_is_new ? getBadgeBlock(t('components.productCart.badge'), badgeColor) : null }
            </Link>
        </div>
    )
}

function getBadgeBlock(title, color) {
    return (
        <span className="tt-label-location">
            <span className="tt-label-sale" style={{background: color}}>{title}</span>
        </span>
    )
}

export default withTranslation()(ImageBoxBlock)