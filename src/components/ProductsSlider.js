import React from 'react';
import Slider from "react-slick";

import ProductCard from "./ProductCard";

export const productInfo = {
    id: 1,
    name: 'Дикий лосось чавыча свежемороженая потрошёная, штучной заморозки, 1 кг',
    slug: 'dikiy_losos',
    categories: [
        'Мороженная рыба'
    ],
    images: [
        {
            "original": 'https://seafood-shop.ru/upload/iblock/874/874e3a90d49c8c19b25e450915e2605a.jpg'
        }
    ],
    example_is_new: false,
    example_is_trendy: false,
    price: {
        amount: 1545,
        currency: "₽"
    }
};

let productsInfo = [1, 2, 3, 4, 5, 6, 7, 8, 9].map((index) => (productInfo));

export default ({ title, count }) => {
    const settings = {
        dots: false,
        lazyLoad: true,
        infinite: true,
        speed: 500,
        slidesToShow: count,
        slidesToScroll: 1,
        initialSlide: 2,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
    };
    let products = productsInfo.map((product) => (
        <ProductCard key={product.slug} product={product} />
    ));
    return (
        <div className="container container-fluid-custom-mobile-padding product-slider" style={{marginTop: "60px"}}>
            <div className="tt-block-title text-left">
                <h1 className="tt-title">{ title }</h1>
            </div>
            <div className="tt-tab-wrapper slider" style={{ marginTop: "-10px" }}>
                <Slider {...settings}>
                    {products}
                </Slider>
            </div>
        </div>)
}

const SamplePrevArrow = (props) => {
    const { className, style, onClick } = props;
    return (
        <button type="button" data-role="none" className="slick-prev slick-arrow"
                aria-label="Previous" role="button" onClick={onClick}>Previous
        </button>
    );
}

const SampleNextArrow = (props) => {
    const { className, style, onClick } = props;
    return (
        <button type="button" data-role="none" className="slick-next slick-arrow"
                aria-label="Next" role="button" onClick={onClick}>Next
        </button>
    );
}


