import React from 'react';
import ButtonCart from './BurronCart';
import ImageBoxBlock from './ImageBoxBlock';
import {Link} from "react-router5";

const ProductCard = ({product}) => {
    const category = product.categories ? product.categories[0] ? product.categories[0].name : "" : "";

    return (
            <div className={"tt-product thumbprod-center"}>
                <Link routeName='catalog.product' routeParams={{categorySlug: 'ice_fish', productSlug: product.slug}}>
                    <ImageBoxBlock { ...product}/>
                </Link>
                <div className="tt-description">
                    <h3 className="tt-tag">
                        <Link routeName='catalog.product' routeParams={{categorySlug: 'ice_fish', productSlug: product.slug}}>
                            Мороженная рыба
                        </Link>
                    </h3>
                    <h2 className="tt-title">
                        <Link routeName='catalog.product' routeParams={{categorySlug: 'ice_fish', productSlug: product.slug}}>
                            {product.name}
                        </Link>
                    </h2>
                    { getPriceBlock(product.price) }
                    <ButtonCart/>
                </div>
            </div>
        )
}

function getPriceBlock(price) {
    return (
        <div className="tt-price">
            <span className="new-price">{price.amount} {price.currency}/шт</span>
            {/*{ sale ? <span className="old-price">{sale}{currency}/шт</span> : null }*/}
        </div>
    )
}

export default ProductCard;