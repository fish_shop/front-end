import React, {Fragment, useState} from 'react'
import Collapse from "react-bootstrap/Collapse";


export default ({children, title, wrapperClassName, titleClassName, contentClassName}) => {
    const [open, setOpen] = useState({open: true});

    return (
        <Fragment>
            <div className={`tt-collapse ${open ? 'open' : ''} ${wrapperClassName}`}>
                <h3 className={`tt-collapse-title ${titleClassName}`} onClick={() => setOpen(!open)} aria-expanded={open}>
                    {title}
                </h3>
                <Collapse in={open}>
                    <div className={`tt-collapse-content ${contentClassName}`}>
                        {children}
                    </div>
                </Collapse>
            </div>
        </Fragment>
    );
}