import React, {Fragment} from "react";
import {withTranslation} from "react-i18next";
import {Link} from "react-router5";
import {connect} from "react-redux";

import icon_social_vk from "../assets/images/icons/icon-social-vk.svg";
import icon_social_viber from "../assets/images/icons/icon-social-viber.svg";
import icon_social_wtsp from "../assets/images/icons/icon-social-wtsp.svg";
import icon_social_ok from "../assets/images/icons/icon-social-ok.svg";
import icon_social_inst from "../assets/images/icons/icon-social-inst.svg";
import Collapse from './Collapse';


const Footer = ({t, categories}) => {
    const cats = categories.filter(category => category.parent_category_id === null);
    const catsHtml = cats.map((category, index) => (
        <li key={index}>
            <Link routeName="catalog.category" routeParams={{slug: category.slug}}>
                {category.name}
            </Link>
        </li>
    ));

    const shopperHtml = <ul className="tt-list">
        <li><a href="">Личный кабинет</a></li>
        <li><a href="">Корзина</a></li>
        <li><a href="">Мои заказы</a></li>
    </ul>;

    const companyHtml = <ul className="tt-list">
        <li><Link routeName='contacts'>Адреса магазинов</Link></li>
        <li><a href="">Доставка и оплата</a></li>
        <li><Link routeName='discounts'>Акции и скидки</Link></li>
        <li><Link routeName='recipes'>Рецепты</Link></li>
    </ul>;

    const addressHtml = <address>
        <p>
            {t('footer.about.about')}
        </p>
        <p className="mt-3">
            <span>{t('footer.about.phone.title')}:</span> {t('phone')}
        </p>
        <p>
            <span>{t('footer.about.address.title')}:</span> {t('footer.about.address.content')}
        </p>
        <p>
            <span>{t('footer.about.email.title')}:</span>
            <a href={"mailto:" + t('footer.about.email')}> {t('footer.about.email.content')}</a>
        </p>
    </address>;

    const socialsHtml = <div className="socials">
        <ul>
            <li>
                <img src={icon_social_vk}/>
            </li>
            <li>
                <img src={icon_social_viber}/>
            </li>
            <li>
                <img src={icon_social_wtsp}/>
            </li>
            <li>
                <img src={icon_social_ok}/>
            </li>
            <li>
                <img src={icon_social_inst}/>
            </li>
        </ul>
    </div>;

    return (
        <Fragment>
            <footer>
                <div className="tt-footer-default tt-color-scheme-02 d-none d-md-flex">
                    <div className="container">
                        <div className="row tt-newsletter">
                            <div className="d-none d-lg-flex col-lg-4">
                                <h4 className="tt-title padding-0 color-white">{t('footer.subscribe.title')}</h4>
                            </div>
                            <div className="col-md-8 col-lg-5 align-items-center justify-content-center">
                                <form id="newsletterform" className="form-inline form-default justify-content-center"
                                      method="post" noValidate="novalidate" action="#">
                                    <div className="form-group">
                                        <input type="text" name="email" className="form-control"
                                               placeholder={t('footer.subscribe.email')}/>
                                        <button type="submit"
                                                className="btn">{t('footer.subscribe.button-text')}</button>
                                    </div>
                                </form>
                            </div>
                            <div className="col-md-4 col-lg-3 justify-content-end d-flex">
                                {socialsHtml}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tt-footer-col tt-color-scheme-01">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-lg-3 col-xl-3">
                                <Collapse title="Категории" wrapperClassName='tt-mobile-collapse d-block d-md-none'>
                                    <ul className="tt-list">
                                        {catsHtml}
                                    </ul>
                                </Collapse>
                                <div className="tt-mobile-collapse d-none d-md-block">
                                    <h4 className="tt-collapse-title">
                                        Все товары
                                    </h4>
                                    <div className="tt-collapse-content">
                                        <ul className="tt-list">
                                            {catsHtml}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-3 col-xl-3">
                                <Collapse title="Покупателям" wrapperClassName='tt-mobile-collapse d-block d-md-none'>
                                    {shopperHtml}
                                </Collapse>
                                <div className="tt-mobile-collapse d-none d-md-block">
                                    <h4 className="tt-collapse-title">
                                        Покупателям
                                    </h4>
                                    <div className="tt-collapse-content">
                                        {shopperHtml}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-3 col-xl-3">
                                <Collapse title='О компании' wrapperClassName='tt-mobile-collapse d-block d-md-none'>
                                    {companyHtml}
                                </Collapse>
                                <div className="tt-mobile-collapse d-none d-md-block">
                                    <h4 className="tt-collapse-title">
                                        О компании
                                    </h4>
                                    <div className="tt-collapse-content">
                                        {companyHtml}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-3 col-xl-3">
                                <div className="tt-newsletter">
                                    <Collapse title={t('footer.about.company-name')}
                                              wrapperClassName='tt-mobile-collapse d-block d-md-none'>
                                        {addressHtml}
                                    </Collapse>
                                    <div className="tt-mobile-collapse d-none d-md-block">
                                        <h4 className="tt-collapse-title">
                                            {t('footer.about.company-name')}
                                        </h4>
                                        <div className="tt-collapse-content">
                                            {addressHtml}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tt-footer-custom">
                    <div className="container">
                        <div className="tt-row">
                            <div className="tt-col-left">
                                <div className="tt-col-item tt-logo-col">
                                    <a className="tt-logo tt-logo-alignment" href="index.html">
                                        {/*<img src="images/custom/logo.png" alt=""/>*/}
                                    </a>
                                </div>
                                <div className="tt-col-item">
                                    <div className="tt-box-copyright">
                                        &copy; 2019 «МАЯК СИБИРИ»
                                    </div>
                                </div>
                            </div>
                            <div className="tt-col-right">
                                <div className="tt-col-item">
                                    <ul className="tt-payment-list">
                                        <li>
                                            <a href="page404.html">
                                            <span className="icon-paypal-2">
                                                <span className="path1"/>
                                                <span className="path2"/>
                                                <span className="path3"/>
                                                <span className="path4"/>
                                                <span className="path5"/>
                                                <span className="path6"/>
                                            </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="page404.html">
                                            <span className="icon-visa">
                                                <span className="path1"/>
                                                <span className="path2"/>
                                                <span className="path3"/>
                                                <span className="path4"/>
                                                <span className="path5"/>
                                            </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="page404.html">
                                            <span className="icon-mastercard">
                                                <span className="path1"/>
                                                <span className="path2"/>
                                                <span className="path3"/>
                                                <span className="path4"/>
                                                <span className="path5"/>
                                                <span className="path6"/>
                                                <span className="path7"/>
                                                <span className="path8"/>
                                                <span className="path9"/>
                                                <span className="path10"/>
                                                <span className="path11"/>
                                                <span className="path12"/>
                                                <span className="path13"/>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </Fragment>
    )
};

const mapStateToProps = state => ({
    categories: state.Catalog.categories,
    route: state.router.route,  // Important!! It is necessary for re-rendering active links on route change.
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(
    withTranslation()(Footer)
);
