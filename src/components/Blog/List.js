import React, {Component} from 'react';
import {withTranslation} from 'react-i18next';

import BlogListItem from '../../components/Blog/ListItem';


class BlogList extends Component {
    render() {
        const listItems = this.props.listItems.map(
            (item, index) => <BlogListItem key={index} item={item} routeName={this.props.routeName}/>
        );

        return (
            <div className="container-indent">
                <div className="container">
                    <h1 className="tt-title-subpages noborder">{this.props.h1}</h1>
                    <div className="tt-blog-masonry">
                        <div className="tt-blog-init tt-grid-col-3 tt-layout-01-post tt-add-item tt-show">
                            {listItems}
                        </div>
                    </div>
                    {/* TODO: запилить кнопку */}
                    {/*<div className="text-center isotop_showmore_js">*/}
                    {/*    <a href="#" className="btn btn-border">Показать еще</a>*/}
                    {/*</div>*/}
                </div>
            </div>
        );
    }
}

export default withTranslation()(BlogList);
