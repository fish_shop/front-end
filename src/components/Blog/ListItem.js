import React from 'react';
import {Link} from "react-router5";


const BlogListItem = ({item, routeName}) => {
    const routeNameFull = `${routeName}.single`;
    return (
        <div className="element-item">
            <div className="tt-post">
                <div className="tt-post-img">
                    <Link routeName={routeNameFull} routeParams={{slug: item.slug}}>
                        <img src={item.image} alt={item.title} />
                    </Link>
                </div>
                <div className="tt-post-content">
                    <div className="tt-background"/>
                    <h2 className="tt-title">
                        <Link routeName={routeNameFull} routeParams={{slug: item.slug}}>
                            {item.title}
                        </Link>
                    </h2>
                    <div className="tt-description">
                        {item.short_description}
                    </div>
                    <div className="tt-meta">
                        <div className="tt-autor">
                            {item.created_at}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BlogListItem;