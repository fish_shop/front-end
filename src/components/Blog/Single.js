import React, {Component} from 'react';
import {withTranslation} from 'react-i18next';
import Col from "react-bootstrap/Col";


class BlogSingle extends Component {
    render() {
        return (
            <div className="container-indent">
                <div className="container">
                    <div className="row justify-content-center">
                        <Col xs={12} md={10} lg={8}>
                            <div className="tt-post-single">
                                <h1 className="tt-title">
                                    {this.props.item.title}
                                </h1>
                                <div className="tt-autor">
                                    {this.props.item.created_at}
                                </div>
                                <div className="tt-post-content">
                                    <Col xs={12}>
                                        <img src={this.props.item.image} alt={this.props.item.title}/>
                                    </Col>

                                    <Col xs={12} dangerouslySetInnerHTML={{__html: this.props.item.content}} />
                                </div>
                            </div>
                        </Col>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTranslation()(BlogSingle);
