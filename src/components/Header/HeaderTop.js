import React, {Component} from "react";
import {connect} from 'react-redux'
import {Link} from 'react-router5'
import {withTranslation} from "react-i18next";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import icon_social_vk from '../../assets/images/icons/icon-social-vk.svg';
import icon_social_wtsp from '../../assets/images/icons/icon-social-wtsp.svg';
import icon_social_viber from '../../assets/images/icons/icon-social-viber.svg';
import icon_social_ok from '../../assets/images/icons/icon-social-ok.svg';
import icon_social_inst from '../../assets/images/icons/icon-social-inst.svg';


class HeaderTop extends Component {
    render() {
        return (
            <Container className="d-none d-md-flex tt-header-holder justify-content-between">
                <div className="tt-desctop-menu">
                    <nav>
                        <ul>
                            <li className="dropdown tt-megamenu-col-02">
                                <Link routeName="home">{this.props.t('header.menuItems.home')}</Link>
                            </li>
                            <li className="dropdown tt-megamenu-col-02">
                                <Link routeName="catalog">{this.props.t('header.menuItems.catalog')}</Link>
                            </li>
                            <li className="dropdown tt-megamenu-col-02">
                                <Link routeName="discounts">{this.props.t('header.menuItems.discounts')}</Link>
                            </li>
                            <li className="dropdown tt-megamenu-col-02">
                                <Link routeName="recipes">{this.props.t('header.menuItems.recipes')}</Link>
                            </li>
                            <li className="dropdown tt-megamenu-col-02">
                                <Link routeName="contacts">{this.props.t('header.menuItems.contacts')}</Link>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div className="socials">
                    <ul>
                        <li>
                            <img src={icon_social_vk}/>
                        </li>
                        <li>
                            <img src={icon_social_viber}/>
                        </li>
                        <li>
                            <img src={icon_social_wtsp}/>
                        </li>
                        <li>
                            <img src={icon_social_ok}/>
                        </li>
                        <li>
                            <img src={icon_social_inst}/>
                        </li>
                    </ul>
                </div>
            </Container>
        )
    };
}

export default connect(state => state.router.route)(withTranslation()(HeaderTop))