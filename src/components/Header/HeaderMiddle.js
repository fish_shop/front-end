import React from 'react';
import {withTranslation} from "react-i18next";
import {Link} from 'react-router5'
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import phoneIcon from '../../assets/images/icons/icon-phone.svg';
import searchIcon from '../../assets/images/icons/icon-search.svg';
import mainLogoSea from '../../assets/images/ShopLogoSea.png'
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";


const HeaderMiddle = (props) => {
    return (
        <div className="bg-white header-middle">
            <Container>
                <Row className="align-items-center">
                    <Col xs={4} className="d-md-none"/>
                    <Col xs={4} className="logo">
                        <Link routeName="home">
                            <img src={mainLogoSea} className="header-middle-logo" />
                        </Link>
                    </Col>
                    <Col md={4} sm={12} className="header-middle-search-input-cntr">
                        <InputGroup className="mb-md-3">
                            <FormControl placeholder="Поиск" type="text" className="header-middle-search-input"/>
                            <InputGroup.Append>
                                <Button className="header-middle-search-btn" >
                                    <img src={searchIcon} alt=""/>
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                    <Col md={4} className="d-none d-md-flex header-middle-contact-phone-cntr">
                        <img src={phoneIcon} alt="" className="header-middle-phone-icon"/>
                        <div className="header-middle-phone-cntr">
                            <div className="header-middle-phone">
                                {props.t('phone')}
                            </div>
                            <div className="header-middle-phone-answer-hours">
                                {props.t('phoneHours')}
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
};

export default withTranslation()(HeaderMiddle);