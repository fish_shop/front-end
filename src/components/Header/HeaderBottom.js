import React from 'react';
import Container from 'react-bootstrap/Container';
import {Link} from "react-router5";
import Category from "../CategoriesMenu/Category";
import defaultIcon from "../../assets/images/icons/burger.svg";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";


const HeaderBottom = ({categories}) => {
    // TODO: имплементировать на бекенде
    // const cats = categories.filter(category => category.is_header === true);
    const cats = categories.filter(category => category.parent_category_id === null);
    const catsHtml = cats.map((category, index) => (
        <Link routeName="catalog.category" routeParams={{slug: category.slug}} className="category text-white" key={index}>
            {category.name}
        </Link>
    ));

    return (
        <div className="header-bottom d-none d-md-flex">
            <Container className="d-flex align-items-center justify-content-between flex-row">
                {catsHtml}

                <Link routeName="discounts" className="category text-white">Акции и скидки</Link>
                <Link routeName="recipes" className="category text-white">Рецепты</Link>
            </Container>
        </div>
    )
};

const mapStateToProps = state => ({
    categories: state.Catalog.categories,
    route: state.router.route,  // Important!! It is necessary for re-rendering active links on route change
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(
    withTranslation()(HeaderBottom)
);
