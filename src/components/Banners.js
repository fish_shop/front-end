import React from 'react';

import banner1 from '../assets/images/placeholders/promo-main-bnr.png';
import banner2 from '../assets/images/placeholders/bnr-s-1.png';
import banner3 from '../assets/images/placeholders/bnr-s-2-2.png';

const Banners = ({ bigImage, smallImageTop, smallImageBottom }) => {
    return (
        <div className="row">
            <div className="col-sm-6 col-md-8">
                <a href="" style={{height: "calc(100% - 20px)"}} className="tt-promo-box tt-one-child">
                    <img src={bigImage} style={{height: "100%"}}
                         alt=""/>
                </a>
            </div>
            <div className="col-sm-6 col-md-4">
                <div className="row">
                    <div className="col-sm-12">
                        <a href="" className="tt-promo-box tt-one-child hover-type-2">
                            <img src={smallImageTop} alt=""/>
                        </a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <a href="" style={{marginTop: "33px"}} className="tt-promo-box tt-one-child hover-type-2">
                            <img src={smallImageBottom} alt=""/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Banners }

export default () => <Banners bigImage={ banner1 } smallImageTop={banner2} smallImageBottom={banner3} />;