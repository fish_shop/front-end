import React, {Component} from 'react';
import BurgerMenu from 'react-burger-menu';

import CategoriesMenu from '../components/CategoriesMenu';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";


class MenuWrap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hidden: false
        };
    }

    componentWillReceiveProps(nextProps) {
        const sideChanged = this.props.children.props.right !== nextProps.children.props.right;

        if (sideChanged) {
            this.setState({hidden: true});

            setTimeout(() => {
                this.show();
            }, this.props.wait);
        }
    }

    show() {
        this.setState({hidden: false});
    }

    render() {
        let style;

        if (this.state.hidden) {
            style = {display: 'none'};
        }

        return (
            <div style={style} className={this.props.side}>
                {this.props.children}
            </div>
        );
    }
}

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentMenu: 'slide',
            side: 'left'
        };
    }

    changeMenu(menu) {
        this.setState({currentMenu: menu});
    }

    changeSide(side) {
        this.setState({side});
    }

    getMenu() {
        const Menu = BurgerMenu[this.state.currentMenu];

        return (
            <MenuWrap wait={20} side={this.state.side}>
                <Menu id={this.state.currentMenu}
                      pageWrapId={'page-wrap'}
                      outerContainerId={'outer-container'}
                      right={this.state.side === 'right'}
                >
                    <Container>
                        <Row>
                            <CategoriesMenu/>
                        </Row>
                    </Container>
                </Menu>
            </MenuWrap>
        );
    }

    render() {
        return this.getMenu()
    }
}

export default Sidebar