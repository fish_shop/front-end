import React from 'react';
import {withTranslation} from 'react-i18next';

const ButtonCart = (props) => (
    <div className="tt-product-inside-hover" style={{ opacity: "1"}}>
        <div className="tt-row-btn">
            <a href="#" className="tt-btn-addtocart thumbprod-button-bg">{props.t('components.productCart.buttonCart')}</a>
        </div>
    </div>
)

export default withTranslation()(ButtonCart)