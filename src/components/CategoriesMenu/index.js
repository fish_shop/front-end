import React from 'react';
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";

import Category from "./Category";


const CategoriesMenu = ({categories, className, showArrows=true}) => {
    const cats = categories.filter(category => category.parent_category_id === null);
    const catsHtml = cats.map((category, index) => (
        <Category key={index} category={category} showArrow={showArrows}/>
    ));

    return (
        <div className={`category-menu ${className}`}>
            {catsHtml}
        </div>
    )
};

const mapStateToProps = state => ({
    categories: state.Catalog.categories,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(
    withTranslation()(CategoriesMenu)
);
