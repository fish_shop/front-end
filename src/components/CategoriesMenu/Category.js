import React from 'react';

import arrowRight from '../../assets/images/icons/arrow-right.svg';
import {Link} from "react-router5";


const Category = ({category, showArrow}) => {
    return (
        <Link routeName="catalog.category" routeParams={{slug: category.slug}} className="menu-category">
            <div className="item-icon-container">
                <img src={category.icon} className="item-icon"/>
            </div>
            {/* TODO: тут должна быть ссылка */}
            <div className="item-caption">{category.name}</div>
            {showArrow ? <img src={arrowRight} className="item-arrow"/> : ''}
        </Link>
    )
};

export default Category;