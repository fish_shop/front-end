import {
    discountListRequestSuccess,
    discountListRequestFail,

    discountBySlugRequestSuccess,
    discountBySlugRequestFail,
} from '../actions/Discounts'

import {
    discountBySlugRequest,
    discountListRequest,
} from '../api/discounts'


export const fetchDiscountList = (limit=20, offset=0) => {
    return function (dispatch) {
        return discountListRequest().then(
            request => dispatch(discountListRequestSuccess(request.data)),
            error => dispatch(discountListRequestFail(error)),
        );
    };
};

export const fetchDiscountBySlug = (slug) => {
    return function (dispatch) {
        return discountBySlugRequest(slug).then(
            request => dispatch(discountBySlugRequestSuccess(request.data)),
            error => dispatch(discountBySlugRequestFail(error)),
        );
    };
};
