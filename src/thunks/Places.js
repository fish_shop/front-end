import {

    placesListRequestSuccess,
    placesListRequestFail
} from '../actions/Places'

import {
    placesListRequest,
} from '../api/places'


export const fetchPlacesList = () => {
    return function (dispatch) {
        return placesListRequest().then(
            (request) => dispatch(placesListRequestSuccess(request.data)),
            error => dispatch(placesListRequestFail(error)),
        );
    };
};
