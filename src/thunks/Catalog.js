import get from 'lodash/get';
import merge from 'lodash/merge';
import find from 'lodash/find';

import {
    categoriesRequestSuccess,
    categoriesRequestFail,

    productsRequestStart,
    productsRequestSuccess,
    productsRequestFail,

    filtersRequestSuccess,
    filtersRequestFail,
} from '../actions/Catalog'
import {
    categoriesRequest,
    productsRequest,
    filtersRequest,
} from '../api/catalog'


export const fetchCategories = () => {
    // We can invert control here by returning a function - the "thunk".
    // When this function is passed to `dispatch`, the thunk middleware will intercept it,
    // and call it with `dispatch` and `getState` as arguments.
    // This gives the thunk function the ability to run some logic, and still interact with the store.
    return function (dispatch) {
        return categoriesRequest().then(
            request => dispatch(categoriesRequestSuccess(request.data)),
            error => dispatch(categoriesRequestFail(error)),
        );
    };
};

export const fetchProducts = () => {
    // We can invert control here by returning a function - the "thunk".
    // When this function is passed to `dispatch`, the thunk middleware will intercept it,
    // and call it with `dispatch` and `getState` as arguments.
    // This gives the thunk function the ability to run some logic, and still interact with the store.
    return function (dispatch, getState) {
        dispatch(productsRequestStart());

        // TODO: костыль, мы вызываем обновление товаров до обновления стейта!
        setTimeout(() => {
            const {form, router, Catalog} = getState();

            const categorySlug = get(router, 'route.name') === 'catalog.category' ? router.route.params.slug : null;
            const category = find(Catalog.categories, (category) => category.slug === categorySlug);
            const categoryParams = category ? {'categories[]': category.id} : {};


            const requestParams = merge(
                get(form.sorting, 'values', {}),
                categoryParams,
            );

            // Добавляем фильтры в запрос
            get(Catalog, 'filters', []).map(filter => {
                switch (filter.type) {
                    case 'categories':
                        // Do nothing
                        break;
                    case 'range':
                        // TODO: implement it
                        break;
                    case 'checkbox':
                        const checkboxes = get(form.filter, `values.${filter.slug}`, {});
                        let itemsSelected = [];
                        for (let key in checkboxes) {
                            if (checkboxes.hasOwnProperty(key)) {
                                if (checkboxes[key]) {
                                    itemsSelected.push(key)
                                }
                            }
                        }
                        if (itemsSelected.length > 0) {
                            requestParams[filter.slug] = itemsSelected;
                        }
                        break;
                    case 'boolean':
                        const value = get(form.filter, `values.${filter.slug}`);
                        if (value === 'yes') {
                            requestParams[filter.slug] = true;
                        }
                        break;
                }
            });

            return productsRequest(requestParams).then(
                request => dispatch(productsRequestSuccess(request.data)),
                error => dispatch(productsRequestFail(error)),
            );
        }, 150);
    };
};

export const fetchFilters = () => {
    // We can invert control here by returning a function - the "thunk".
    // When this function is passed to `dispatch`, the thunk middleware will intercept it,
    // and call it with `dispatch` and `getState` as arguments.
    // This gives the thunk function the ability to run some logic, and still interact with the store.
    return function (dispatch) {
        return filtersRequest().then(
            request => dispatch(filtersRequestSuccess(request.data)),
            error => dispatch(filtersRequestFail(error)),
        );
    };
};
