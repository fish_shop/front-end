import {
    recipesListRequestSuccess,
    recipesListRequestFail,

    recipeBySlugRequestSuccess,
    recipeBySlugRequestFail,
} from '../actions/Recipes'

import {
    recipeBySlugRequest,
    recipesListRequest,
} from '../api/recipes'


export const fetchRecipesList = (limit=20, offset=0) => {
    return function (dispatch) {
        return recipesListRequest(limit, offset).then(
            request => dispatch(recipesListRequestSuccess(request.data)),
            error => dispatch(recipesListRequestFail(error)),
        );
    };
};

export const fetchRecipeBySlug = (slug) => {
    return function (dispatch) {
        return recipeBySlugRequest(slug).then(
            request => dispatch(recipeBySlugRequestSuccess(request.data)),
            error => dispatch(recipeBySlugRequestFail(error)),
        );
    };
};
