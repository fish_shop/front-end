import {
    DISCOUNT_LIST_REQUEST_SUCCESS,
    DISCOUNT_BY_SLUG_REQUEST_SUCCESS,
} from '../actions/Discounts';


const defaultState = {
    list: [],
    single: undefined,
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case DISCOUNT_LIST_REQUEST_SUCCESS:
            return {
                ...state,
                list: action.items,
            };
        case DISCOUNT_BY_SLUG_REQUEST_SUCCESS:
            return {
                ...state,
                single: action.data,
            };

    }
    return state;
};
