import {
    RECIPES_LIST_REQUEST_SUCCESS,
    RECIPE_BY_SLUG_REQUEST_SUCCESS,
} from '../actions/Recipes';


const defaultState = {
    list: [],
    single: undefined,
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case RECIPES_LIST_REQUEST_SUCCESS:
            return {
                ...state,
                list: action.items,
            };
        case RECIPE_BY_SLUG_REQUEST_SUCCESS:
            return {
                ...state,
                single: action.data,
            };

    }
    return state;
};
