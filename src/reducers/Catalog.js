import {
    CATEGORIES_REQUEST_SUCCESS,
    FILTERS_REQUEST_SUCCESS, PRODUCTS_REQUEST_FAIL, PRODUCTS_REQUEST_START,
    PRODUCTS_REQUEST_SUCCESS,
} from '../actions/Catalog';


const defaultState = {
    categories: [],
    products: [],
    isLoadingProducts: false,
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case CATEGORIES_REQUEST_SUCCESS:
            return {
                ...state,
                categories: action.categories,
            };
        case PRODUCTS_REQUEST_START:
            return {
                ...state,
                isLoadingProducts: true,
            };
        case PRODUCTS_REQUEST_SUCCESS:
            return {
                ...state,
                products: action.products,
                isLoadingProducts: false,
            };
        case PRODUCTS_REQUEST_FAIL:
            return {
                ...state,
                isLoadingProducts: false,
            };
        case FILTERS_REQUEST_SUCCESS:
            return {
                ...state,
                filters: action.filters,
            };
    }
    return state;
};
