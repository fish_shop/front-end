import {combineReducers} from 'redux';
import {router5Reducer} from 'redux-router5';
import { reducer as formReducer } from 'redux-form'

import Catalog from './Catalog';
import Discounts from './Discounts';
import Recipes from './Recipes';
import Contacts from './Contacts';



export default combineReducers({
    router: router5Reducer,
    form: formReducer,

    Catalog,
    Discounts,
    Recipes,
    Contacts
});
