import {
    PLACES_LIST_REQUEST_SUCCESS
} from '../actions/Places';

const defaultState = {
    shops: []
};

export default function reducer(state = defaultState, action) {

    switch (action.type) {
        case PLACES_LIST_REQUEST_SUCCESS:
            return {
                ...state,
                shops: [...action.items],
            };
        }

    return state;
}



