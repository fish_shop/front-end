import React from 'react';

import iconMarketPlace from '../../assets/images/placeholders/icon-location.svg'
import iconDelivery from '../../assets/images/placeholders/icon-delivery.svg'
import iconMoneyback from '../../assets/images/placeholders/icon-moneyback.svg'
import iconGift from '../../assets/images/placeholders/icon-gift.svg'
import {withTranslation} from "react-i18next";

const Advantages = (props) => (
    <div className="container-indent advantage">
        <div className="container">
            <div className="row tt-services-listing">
                <div className="advantage-block col-xs-12 col-md-6 col-lg-3">
                    <div className="tt-col-icon">
                        <img src={iconDelivery} className="advantage-icon" />
                    </div>
                    <div className="tt-col-description">
                        <p>{props.t('pages.home.advantages.delivery')}</p>
                    </div>
                </div>
                <div className="advantage-block col-xs-12 col-md-6 col-lg-3">
                    <div className="tt-col-icon">
                        <img src={iconMarketPlace} className="advantage-icon" />
                    </div>
                    <div className="tt-col-description">
                        <p>{props.t('pages.home.advantages.place')}</p>
                    </div>
                </div>
                <div className="advantage-block col-xs-12 col-md-6 col-lg-3">
                    <div className="tt-col-icon">
                        <img src={iconMoneyback} className="advantage-icon" />
                    </div>
                    <div className="tt-col-description">
                        <p>{props.t('pages.home.advantages.refund')}</p>
                    </div>
                </div>
                <div className="advantage-block col-xs-12 col-md-6 col-lg-3">
                    <div className="tt-col-icon">
                        <img src={iconGift} className="advantage-icon" />
                    </div>
                    <div className="tt-col-description">
                        <p>{props.t('pages.home.advantages.discount')}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

export default withTranslation()(Advantages)