import React from 'react';
import {withTranslation} from "react-i18next";


const TopReviews = (props) => (
    <div className="tt-review-block">
        <div className="tt-row-custom-02">
            <div className="col-item">
                <h2 className="tt-title">
                    {props.t('pages.home.topReviews')}
                </h2>
            </div>
        </div>
        <div className="tt-review-comments">
            <div className="tt-item">
                <div className="tt-avatar">
                    <a href="#"><img src="images/product/single/review-comments-img-01.jpg" alt=""
                                     className="loading" data-was-processed="true"/></a>
                </div>
                <div className="tt-content">
                    <div className="tt-rating">
                        <i className="icon-star"></i>
                        <i className="icon-star"></i>
                        <i className="icon-star"></i>
                        <i className="icon-star-half"></i>
                        <i className="icon-star-empty"></i>
                    </div>
                    <div className="tt-comments-info">
                        <span className="username"><span>Павел Андреевич</span></span>
                        <span className="time">15 мая 2019</span>
                    </div>
                    <div className="tt-comments-title">Горбуша очень хорошая</div>
                    <p>
                        Горбуша очень хорошая. Голова и хвост идут на суп, а остальное на стейки.
                    </p>
                </div>
            </div>
            <div className="tt-item">
                <div className="tt-avatar">
                    <a href="#"><img src="images/product/single/review-comments-img-02.jpg" alt=""
                                     className="loading" data-was-processed="true"/></a>
                </div>
                <div className="tt-content">
                    <div className="tt-rating">
                        <i className="icon-star"></i>
                        <i className="icon-star"></i>
                        <i className="icon-star"></i>
                        <i className="icon-star-half"></i>
                        <i className="icon-star-empty"></i>
                    </div>
                    <div className="tt-comments-info">
                        <span className="username"><span>Анна</span></span>
                        <span className="time">9 апреля 2019</span>
                    </div>
                    <div className="tt-comments-title">Икра щуки астраханская</div>
                    <p>
                        купила первый раз "на пробу"; оказалось, вкус чем-то напоминает черную икру
                    </p>
                </div>
            </div>
            <div className="tt-item">
                <div className="tt-avatar">
                    <a href="#"></a>
                </div>
                <div className="tt-content">
                    <div className="tt-rating">
                        <i className="icon-star"></i>
                        <i className="icon-star"></i>
                        <i className="icon-star"></i>
                        <i className="icon-star-half"></i>
                        <i className="icon-star-empty"></i>
                    </div>
                    <div className="tt-comments-info">
                        <span className="username"><span>Павел Андреевич</span></span>
                        <span className="time">10 января 2019</span>
                    </div>
                    <p>
                        Всегда свежая рыба, еще ни разу не подвели
                    </p>
                </div>
            </div>
        </div>
    </div>
)

export default withTranslation()(TopReviews)