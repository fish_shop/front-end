import React from 'react';
import {withTranslation} from "react-i18next";
import {Link} from "react-router5";
import {connect} from "react-redux";


const TopDiscounts = (props) => {
    const discounts = props.discountList ? props.discountList.map(discount => <div className="tt-item">
            <div className="tt-content">
                <div className="tt-comments-info">
                    <span className="time">{discount.created_at}</span>
                </div>
                <div className="tt-comments-title">{discount.title}</div>
                <p>{discount.short_description}</p>
            </div>
        </div>
    ) : '';

    return (
        <div className="tt-review-block">
            <div className="tt-row-custom-02">
                <div className="col-item">
                    <h2 className="tt-title">
                        {props.t('pages.home.topDiscounts.title')}
                    </h2>
                </div>
                <div className="col-item">
                    <Link routeName="discounts">{props.t('pages.home.topDiscounts.goToPage')}</Link>
                </div>
            </div>
            <div className="tt-review-comments">
                {discounts}
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    discountList: state.Discounts.list,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(TopDiscounts));
