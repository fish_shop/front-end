import React from 'react';
import {withTranslation} from "react-i18next";
import Container from "react-bootstrap/Container";
import popularCategory1 from "../../assets/images/tmp/steak.jpg";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import {Link} from "react-router5";
import {connect} from "react-redux";


const PopularCategories = (props) => {
    const popularCats = props.categories ? props.categories.filter(category => category.is_popular) : [];
    const categories = popularCats.map((category, idx) => (
        <Col xs={6} sm={3} key={idx}>
            <Link routeName="catalog.category" routeParams={{slug: category.slug}}
                  className="tt-promo-box tt-one-child hover-type-2 height170"
                  style={{backgroundImage: `url(${popularCategory1})`}}>
                <img src={category.image} alt={category.name}/>
                <div className="tt-description">
                    <div className="tt-description-wrapper">
                        <div className="tt-background"/>
                        <div className="tt-title-small">{category.name}</div>
                    </div>
                </div>
            </Link>
        </Col>
    ));

    return (
        <Container style={{marginTop: "60px"}}>
            <div className="tt-layout-promo-box">
                <div className="tt-block-title text-left" style={{paddingBottom: 0}}>
                    <h2 className="tt-title"
                        style={{fontSize: "24px"}}>{props.t('pages.home.popularCategories.title')}</h2>
                </div>
                <Row>
                    {/*<div className="col-sm-6 col-md-3 d-none d-md-block">*/}
                    {/*    <a href="" className="left-side-fullsize-container tt-promo-box tt-one-child">*/}
                    {/*        <div className="tt-description" style={{backgroundImage: `url(${popularCategory0})`}}>*/}
                    {/*            <div className="tt-description-wrapper">*/}
                    {/*                <div className="tt-background" />*/}
                    {/*                <div className="tt-title-small">Рыба</div>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*    </a>*/}
                    {/*</div>*/}
                    {/*<div className="col-sm-12 col-md-9">*/}
                    {categories}
                    {/*<div className="row">*/}
                    {/*    <div className="col-sm-4">*/}
                    {/*        <a href="" className="tt-promo-box tt-one-child hover-type-2 height170" style={{backgroundImage: `url(${popularCategory3})`}}>*/}
                    {/*            <div className="tt-description">*/}
                    {/*                <div className="tt-description-wrapper">*/}
                    {/*                    <div className="tt-background"></div>*/}
                    {/*                    <div className="tt-title-small">Филе</div>*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </a>*/}
                    {/*    </div>*/}
                    {/*    <div className="col-sm-4">*/}
                    {/*        <a href="" className="tt-promo-box tt-one-child hover-type-2 height170" style={{backgroundImage: `url(${popularCategory4})`}}>*/}
                    {/*            <div className="tt-description">*/}
                    {/*                <div className="tt-description-wrapper">*/}
                    {/*                    <div className="tt-background"></div>*/}
                    {/*                    <div className="tt-title-small">Крабы</div>*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </a>*/}
                    {/*    </div>*/}
                    {/*    <div className="col-sm-4">*/}
                    {/*        <a href="" className="tt-promo-box tt-one-child hover-type-2 height170" style={{backgroundImage: `url(${popularCategory5})`}}>*/}
                    {/*            <div className="tt-description">*/}
                    {/*                <div className="tt-description-wrapper">*/}
                    {/*                    <div className="tt-background"></div>*/}
                    {/*                    <div className="tt-title-small">Креветки</div>*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </a>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </Row>
            </div>
        </Container>
    );
};

const mapStateToProps = state => ({
    categories: state.Catalog.categories,
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(PopularCategories));
