import React from 'react';

import recipeImage from "../../assets/images/bnr-1.png"
import {withTranslation} from "react-i18next";


const Recipes = (props) => (
    <div className="container" style={{marginTop: "50px"}}>
        <div className="tt-block-title text-left">
            <h2 className="tt-title" style={{fontSize: "24px"}}>{props.t('pages.home.recipes.title')}</h2>
        </div>
        <div className="tt-blog-thumb-list">
            <div className="row">
                <div className="col-12 col-sm-6 col-md-6 col-lg-4">
                    <div className="tt-blog-thumb">
                        <div className="tt-img"><a href="" target="_blank"><img
                            src={recipeImage}
                            data-src="images/blog/blog-post-img-06.jpg" alt="" className="loaded"
                            data-was-processed="true"/></a></div>
                        <div className="tt-title-description">
                            <div className="tt-background"></div>
                            <div className="tt-tag">
                                <a href="">Главные блюда</a>
                            </div>
                            <div className="tt-title">
                                <a href="">Щука запеченная в сметане</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-sm-6 col-md-6 col-lg-4">
                    <div className="tt-blog-thumb">
                        <div className="tt-img"><a href="" target="_blank"><img
                            src={recipeImage}
                            data-src="images/blog/blog-post-img-04.jpg" alt="" className="loaded"
                            data-was-processed="true"/></a></div>
                        <div className="tt-title-description">
                            <div className="tt-background"></div>
                            <div className="tt-tag">
                                <a href="">Вторые блюда</a>
                            </div>
                            <div className="tt-title">
                                <a href="">Фетучини с морским коктейлем</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-sm-6 col-md-6 col-lg-4">
                    <div className="tt-blog-thumb">
                        <div className="tt-img"><a href="" target="_blank">
                            <img
                            src={recipeImage}
                            data-src="images/blog/blog-post-img-02.jpg" alt="" className="loaded"
                            data-was-processed="true"/></a></div>
                        <div className="tt-title-description">
                            <div className="tt-background"></div>
                            <div className="tt-tag">
                                <a href="">Первые блюда</a>
                            </div>
                            <div className="tt-title">
                                <a href="">Ароматный тыквенный крем-суп с мясом краба</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

export default withTranslation()(Recipes)