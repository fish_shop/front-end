import React from 'react';
import {withTranslation} from "react-i18next";
import {connect} from 'react-redux';

import ProductsSlider from '../../components/ProductsSlider';
import Advantages from './Advantages';
import PopularCategories from './PopularCategories';
import TopReviews from './TopReviews';
import TopDiscounts from './TopDiscounts';
import Banners from "../../components/Banners";
import CategoriesMenu from "../../components/CategoriesMenu";
import BlogList from "../../components/Blog/List";


const Home = (props) => {
    const countSlider = 4;

    return (
        <div className="container-indent">
            <div className="container-indent0">
                <div className="container">
                    <div className="row tt-layout-promo-box">
                        <div className="col-md-3 categories-menu-desktop">
                            <CategoriesMenu/>
                        </div>
                        <div className="col-md-9">
                            <Banners/>
                        </div>
                    </div>
                </div>
            </div>

            <Advantages/>

            <ProductsSlider title={props.t('pages.home.novelties')} count={countSlider}/>

            <ProductsSlider title={props.t('pages.home.promotionsAndDiscounts')} count={countSlider}/>

            <div className="container-indent0">
                <PopularCategories/>
            </div>

            <div className="container" style={{marginTop: "60px"}}>
                <div className="tt-block-title text-left" style={{paddingBottom: 0}}>
                    <h2 className="tt-title" style={{fontSize: "24px"}}>Рецепты</h2>
                </div>
            </div>
            <BlogList
                h1=""
                routeName="recipes"
                listItems={props.recipesList}
            />

            <div className="container" style={{marginTop: '70px'}}>
                <div className="row">
                    <div className="col-md-6">
                        <TopReviews/>
                    </div>
                    <div className="col-md-6">
                        <TopDiscounts/>
                    </div>
                </div>
            </div>

        </div>
    )
};

const mapStateToProps = state => ({
    recipesList: state.Recipes.list,
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Home));
