import React from 'react'
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import ProductCard from "../../../components/ProductCard";
import {connect} from "react-redux";


const ProductsList = ({products, isLoadingProducts}) => {
    let productsBlocks = products.map((product, index) => (
        <Col md={4} xs={6} key={index} className="tt-col-item">
            <ProductCard product={product}/>
        </Col>
    ));

    return (
        <Row className="tt-product-listing" style={{opacity: isLoadingProducts ? 0.5 : 1}}>
            {productsBlocks}
        </Row>
    )
};

const mapStateToProps = state => ({
    products: state.Catalog.products,
    isLoadingProducts: state.Catalog.isLoadingProducts,
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps())(ProductsList);
