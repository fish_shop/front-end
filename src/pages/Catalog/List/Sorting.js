import React, {Component} from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";

import {fetchProducts} from "../../../thunks/Catalog";


class SortingForm extends Component {
    render = () => {
        const {handleSubmit, pristine, reset, submitting} = this.props;

        return (
            <div className="tt-filters-options">
                <h1 className="tt-title">
                    Каталог
                    {/* TODO: вернуть кол-во товаров */}
                    {/*<span className="tt-title-total">(69)</span>*/}
                </h1>
                <div className="tt-btn-toggle">
                    <a href="#">FILTER</a>
                </div>

                <form onSubmit={handleSubmit} className="tt-sort">
                    Сортировать по: {' '}
                    <Field name="order_by" component="select" onChange={this.props.fetchProducts}>
                        <option value="price_asc">Увеличению цены</option>
                        <option value="price_desc">Уменьшению цены</option>
                        <option value="name">Названию</option>
                    </Field>
                </form>
            </div>
        );
    };
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(
    reduxForm({form: 'sorting'})(
        SortingForm
    )
);
