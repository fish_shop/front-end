import React, {Component, Fragment} from 'react';
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";
import Button from "react-bootstrap/Button";

import Collapse from '../../../../components/Collapse';
import {fetchProducts} from "../../../../thunks/Catalog";
import CategoriesMenu from "../../../../components/CategoriesMenu";


class FilterForm extends Component {
    render = () => {
        const {handleSubmit, pristine, reset, submitting} = this.props;

        const resetAndUpdateProducts = () => {
            reset();
            this.props.fetchProducts();
        };

        const filtersHtml = this.props.filters ? this.props.filters.map(filter => {
            switch (filter.type) {
                case 'categories':
                    return <Collapse title={filter.name} contentClassName='mt-0' titleClassName='border-0'>
                        <CategoriesMenu className='mt-0' showArrows={false}/>
                    </Collapse>;
                // case 'range':
                //     return <Collapse title={filter.name}>
                //         <span>TODO: фильтр range</span>
                //     </Collapse>;
                case 'checkbox':
                    const choices = filter.data.map(option => (
                        <li>
                            <label>
                                <Field
                                    name={`${filter.slug}[${option}]`}
                                    component="input"
                                    type="checkbox"
                                    value={option}
                                    onChange={this.props.fetchProducts}
                                />{' '}
                                {option ? option : 'Не указано'}
                            </label>
                        </li>
                    ));
                    return <Collapse title={filter.name}>
                        <Fragment>
                            <ul className="tt-filter-list">
                                {choices}
                            </ul>
                            {/*<a href="#" className="btn-link-02">Сбросить фильтр</a>*/}
                        </Fragment>
                    </Collapse>;
                case 'boolean':
                    return <Collapse title={filter.name}>
                        <Fragment>
                            <ul className="tt-filter-list">
                                <li>
                                    <label>
                                        <Field
                                            name={filter.slug}
                                            component="input"
                                            type="radio"
                                            value='yes'
                                            onChange={this.props.fetchProducts}
                                        />{' '}
                                        {filter.name}
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <Field
                                            name={filter.slug}
                                            component="input"
                                            type="radio"
                                            value='all'
                                            onChange={this.props.fetchProducts}
                                        />{' '}
                                        Все товары
                                    </label>
                                </li>
                            </ul>
                        </Fragment>
                    </Collapse>;
            }
        }) : '';

        return (
            <form onSubmit={handleSubmit}>
                {filtersHtml}

                <Button variant={submitting || pristine ? 'light' : 'primary'} onClick={resetAndUpdateProducts}
                        disabled={submitting || pristine}>
                    Сбросить фильтр
                </Button>
            </form>
        )
    };
}

const mapStateToProps = state => ({
    filters: state.Catalog.filters,
    // TODO: можно прокинуть только в фильтр категорий, чтобы не перерисовывать весь компонент каждый раз
    route: state.router.route,  // Important!! It is necessary for re-rendering active links on route change.
});

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(
    reduxForm({form: 'filter'})(
        FilterForm
    )
);
