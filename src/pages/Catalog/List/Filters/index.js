import React, {Fragment} from 'react';
import FilterForm from './FilterForm'

export default () => (
    <Fragment>
        <div className="tt-btn-col-close">
            <a href="#">Close</a>
        </div>
        <div className="tt-collapse open tt-filter-detach-option">
            <div className="tt-collapse-content">
                <div className="filters-mobile">
                    <div className="filters-row-select">

                    </div>
                </div>
            </div>
        </div>

        <FilterForm />
    </Fragment>
)

