import React, {Component} from 'react';
import {withTranslation} from 'react-i18next';

import Filters from "./Filters";
import Sorting from "./Sorting";
import ProductList from "./ProductList";


class Catalog extends Component {
    render() {
        return (
            <div className="container-indent">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 col-lg-3 col-xl-3 leftColumn aside">
                            <Filters/>
                        </div>
                        <div className="col-md-12 col-lg-9 col-xl-9">
                            <div className="content-indent container-fluid-custom-mobile-padding-02">
                                <Sorting />
                                <ProductList/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTranslation()(Catalog);
