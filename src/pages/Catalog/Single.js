import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import {fetchProducts} from "../../thunks/Catalog";

import {productInfo} from '../../components/ProductsSlider';
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";


class SingleProduct extends Component {
    render() {
        const product = productInfo;

        return (
            <div className="container-indent">
                <Container>
                    <Row>
                        <Col sm={12} md={6} className="justify-content-center">
                            <img className="img-fluid" src={product.images[0].original} alt="" style={{maxWidth: '100%'}}/>
                        </Col>
                        <Col sm={12} md={6}>
                            <div className="tt-product-single-info">
                                <div className="tt-add-info">
                                    <ul>
                                        <li><span>Артикул:</span> 117452</li>
                                    </ul>
                                </div>
                                <h1 className="tt-title">{product.name}</h1>

                                <div className="tt-review">
                                    <div className="tt-rating">
                                        <i className="icon-star"></i>
                                        <i className="icon-star"></i>
                                        <i className="icon-star"></i>
                                        <i className="icon-star"></i>
                                        <i className="icon-star-empty"></i>
                                    </div>
                                    <a className="product-page-gotocomments-js" href="#">(1 отзыв)</a>
                                </div>

                                <div className="tt-wrapper tt-price">
                                    {/*<div className="old-price">1931 ₽</div>*/}
                                    <span className="new-price">{product.price.amount} ₽</span>
                                </div>

                                <div className="tt-wrapper">
                                    <div className="tt-row-custom-01">
                                        <div className="col-item">
                                            <div className="tt-input-counter style-01">
                                                <span className="minus-btn"></span>
                                                <input type="text" value="1" size="5"/>
                                                <span className="plus-btn"></span>
                                            </div>
                                        </div>
                                        <div className="col-item">
                                            <a href="#" className="btn btn-lg"><i className="icon-f-47"/>Добавить в
                                                корзину</a>
                                        </div>
                                    </div>
                                </div>

                                <div className="tt-wrapper">
                                    <div className="tt-add-info">
                                        <ul>
                                            <li>Вам нужна быстрая доставка? &nbsp; <i className="icon-e-35"/>
                                                <strong><i>Условия доставки</i></strong></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col sm={12} md={8}>
                            <h2 className="tt-title mb-3">Описание</h2>
                            <p>Чавычу ценят во всем мире: в Японии она удостоена звания «Князь лососей», а в
                                Америке ее именуют «Королевой рыб». Узнать ее легко по мясу насыщенного
                                красного цвета: очень похожее по вкусу на семгу, но ценится значительно
                                больше.</p>
                            <p>Выловленная в экологически чистых дальневосточных водах и замороженная на
                                судне рыба полностью сохраняет свои вкусовые и полезные качества. Купить
                                чавычу, свежемороженую можно целую, уже потрошеную - это удобно и
                                выгодно.</p>

                            <h2 className="tt-title mt-5 mb-3">Характеристики</h2>
                            <table className="tt-table-02">
                                <tbody>
                                <tr>
                                    <td>Вес упаковки:</td>
                                    <td>3 - 7 кг (1 шт.)</td>
                                </tr>
                                <tr>
                                    <td>Срок хранения:</td>
                                    <td>12 месяцев</td>
                                </tr>
                                <tr>
                                    <td>Температура хранения:</td>
                                    <td>не выше -18С</td>
                                </tr>
                                </tbody>
                            </table>

                            <h2 className="tt-title mt-5 mb-3">Отзывы покупателей (1)</h2>
                            <div className="tt-review-block">
                                <div className="tt-review-comments">
                                    <div className="tt-item">
                                        <div className="tt-avatar">
                                            <a href="#"><img
                                                src="images/product/single/review-comments-img-01.jpg"
                                                alt=""/></a>
                                        </div>
                                        <div className="tt-content">
                                            <div className="tt-rating">
                                                <i className="icon-star"></i>
                                                <i className="icon-star"></i>
                                                <i className="icon-star"></i>
                                                <i className="icon-star"></i>
                                                <i className="icon-star-empty"></i>
                                            </div>
                                            <div className="tt-comments-info">
                                                <span className="username"><span>Павел Андреевич</span></span> &nbsp;
                                                <span className="time">03 августа 2019г.</span>
                                            </div>
                                            <div className="tt-comments-title">Очень вкусно!</div>
                                            <p>
                                                Рыбка очень вкусная и свежая! Всей семьей едим и ни нарадуемся!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col sm={12} md={4}>

                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    product: state.Catalog.single,
});

export default connect(mapStateToProps,
    {fetchProducts})(
    withTranslation()(SingleProduct)
);
