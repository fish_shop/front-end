import React, {Fragment} from 'react';
import {YMaps, Map, Placemark} from "react-yandex-maps";

import iconFish from '../../assets/images/icons/fish.svg'


const mapData = {
    center: [55.053970, 82.893732],
    zoom: 10,
    controls: ['zoomControl'],
    minZoom: 9,
    maxZoom: 14
};

const Maps = (props) => {
    return (
        <Fragment>
            <h4 className="mt-5"> Торговые точки </h4>
            <div style={{height: 400, backgroundColor: '#eee'}} className='mb-3'>
                <YMaps>
                    <Map defaultState={mapData} modules={['control.ZoomControl']} className="map">
                        {
                            props.shops.map((shop, index) =>
                                <Placemark geometry={[shop.longitude, shop.latitude]} key={index}
                                           modules={['geoObject.addon.balloon']}
                                           options={{
                                               iconLayout: 'default#image',
                                               iconImageHref: iconFish,
                                               iconImageSize: [55, 75],
                                               iconImageOffset: [-3, -42]
                                           }}
                                           properties={{
                                               balloonContentHeader:
                                               shop.title,
                                               balloonContentBody:
                                               shop.address,
                                               balloonContentFooter:
                                               shop.opening_hours
                                           }}
                                />
                            )
                        }
                    </Map>
                </YMaps>
            </div>
        </Fragment>
    )
};

export default Maps