import React, {Fragment} from 'react';

import iconGeo from '../../assets/images/icons/icon-geo.svg';
import iconPhone from '../../assets/images/icons/icon-call.svg';
import iconMail from '../../assets/images/icons/icon-mail.svg';
import iconClock from '../../assets/images/icons/icon-clock.svg';
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";


const ContactData = ({contacts}) => {
    return (
        <Fragment>
            <Row className="wrapper">
                <Col xs={12} sm={6} md={6} lg={3} className="wrapper-icon pb-3">
                    <img src={iconGeo} className="icon-back"/>
                    <div className="pl-2">г.Новосибирск, ул.&nbsp;Титова,&nbsp;1</div>
                </Col>
                <Col xs={12} sm={6} md={6} lg={3} className="wrapper-icon pb-3">
                    <a href="phoneto:+79139849686" className="d-flex align-items-center d-lg-none">
                        <img src={iconPhone} className="icon-back"/>
                        <div className="pl-2">+7 (913) 984-96-86</div>
                    </a>
                    <img src={iconPhone} className="icon-back d-none d-lg-inline-block"/>
                    <div className="pl-2 d-none d-lg-inline-block">+7 (913) 984-96-86</div>
                </Col>
                <Col xs={12} sm={6} md={6} lg={3} className="wrapper-icon pb-3">
                    <a href="mailto:client@seafoods-group.ru" className="d-flex align-items-center">
                        <img src={iconMail} className="icon-back"/>
                        <div className="pl-2">client@seafoods-group.ru</div>
                    </a>
                </Col>
                <Col xs={12} sm={6} md={6} lg={3} className="wrapper-icon pb-3">
                    <img src={iconClock} className="icon-back"/>
                    <div className="pl-2">Принимаем заказы ежедневно с 9:00 до 19:00</div>
                </Col>
            </Row>
        </Fragment>
    )
}
export default ContactData;