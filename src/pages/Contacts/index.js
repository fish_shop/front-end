import React from 'react';
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import ContactData from './ContactData'
import Places from './Places'
import Maps from './Maps'
import SocialNet from './SocialNet';

class Contacts extends React.Component {

    render() {
        let {contacts, shops } = this.props;
        return (
            <div className="container">
                <h1 className="tt-title-subpages noborder text-left mt-5">Контакты</h1>
                <ContactData contacts={contacts}/>
                <Maps shops = { shops }/>
                <Places shops = { shops }/>
                <SocialNet/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    contacts: state.Contacts.contacts,
    shops: state.Contacts.shops,
});

export default connect(mapStateToProps, null)(
    withTranslation()(Contacts)
);