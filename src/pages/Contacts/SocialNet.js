import React from 'react';

import icon_social_vk from '../../assets/images/icons/icon-social-vk.svg';
import icon_social_wtsp from '../../assets/images/icons/icon-social-wtsp.svg';
import icon_social_viber from '../../assets/images/icons/icon-social-viber.svg';
import icon_social_ok from '../../assets/images/icons/icon-social-ok.svg';
import icon_social_inst from '../../assets/images/icons/icon-social-inst.svg';


const SocialNet = ({shop}) => {
    return (
        <div>
            <h4 className="mt-5"> Мы в социальных сетях</h4>
            <div className="social-net">
                <ul>
                    <a href="">
                        <li>
                            <img src={icon_social_vk}/>
                        </li>
                    </a>
                    <a href="">
                        <li>
                            <img src={icon_social_viber}/>
                        </li>
                    </a>
                    <a href="">
                        <li>
                            <img src={icon_social_wtsp}/>
                        </li>
                    </a>
                    <a href="">
                        <li>
                            <img src={icon_social_ok}/>
                        </li>
                    </a>
                    <a href="">
                        <li>
                            <img src={icon_social_inst}/>
                        </li>
                    </a>
                </ul>

            </div>
        </div>
    )
}
export default SocialNet