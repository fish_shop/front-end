import React from 'react';

import shopsIcon from '../../../assets/images/icons/shop.svg'

const SinglePlace = ({shop}) => {
    return (
        <div className="col-md-4 col-lg-4 my-3 my-md-4 wrapper-shop">
            <div>
                <img src={shopsIcon}/>
            </div>
            <div className="pl-2">
                <div><strong>{shop.title}</strong></div>
                <div className="underline">{shop.address}</div>
                <div>{shop.opening_hours}</div>
            </div>
        </div>
    )
};

export default SinglePlace