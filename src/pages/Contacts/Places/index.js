import React from 'react';
import SinglePlace from './SinglePlace'



const Shops = ({shops}) => {
    return (
        <div className = "row"> 
            {shops.map((shop, index) => {
                return <SinglePlace shop = {shop} key={index}
                         /> 
                    })
            }
        </div>
    )
};

export default Shops