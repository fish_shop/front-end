import {fetchCategories, fetchFilters, fetchProducts} from './thunks/Catalog';
import {fetchDiscountBySlug, fetchDiscountList} from './thunks/Discounts';
import {fetchRecipeBySlug, fetchRecipesList} from './thunks/Recipes';
import {fetchPlacesList} from './thunks/Places';


export default [
    {
        name: 'home',
        path: '/',
        onActivate: () => function (dispatch, getState) {
            return Promise.all([
                fetchCategories()(dispatch, getState),  // TODO: Crutch: we need fetch categories once on Server side
                fetchRecipesList(3)(dispatch, getState),
                fetchDiscountList(3)(dispatch, getState),
            ])
        },
    },
    {
        name: 'catalog',
        path: '/catalog',
        onActivate: () => function (dispatch, getState) {
            return Promise.all([
                fetchCategories()(dispatch, getState),  // TODO: Crutch: we need fetch categories once on Server side
                fetchProducts()(dispatch, getState),
                fetchFilters()(dispatch, getState),
            ])
        },
    },
    {
        name: 'catalog.category',
        path: '/:slug',
        onActivate: () => function (dispatch, getState) {
            return Promise.all([
                fetchProducts()(dispatch, getState),
            ])
        },
    },
    {
        name: 'catalog.product',
        path: '/:categorySlug/:productSlug',
        onActivate: () => function (dispatch, getState) {
            return Promise.all([
                fetchCategories()(dispatch, getState),  // TODO: Crutch: we need fetch categories once on Server side
            ])
        },
    },
    {
        name: 'discounts',
        path: '/discounts',
        onActivate: () => function (dispatch, getState) {
            return Promise.all([
                fetchCategories()(dispatch, getState),  // TODO: Crutch: we need fetch categories once on Server side
                fetchDiscountList()(dispatch, getState),
            ])
        },
    },
    {
        name: 'discounts.single',
        path: '/:slug',
        onActivate: (params) => function (dispatch, getState) {
            return Promise.all([
                fetchCategories()(dispatch, getState),  // TODO: Crutch: we need fetch categories once on Server side
                fetchDiscountBySlug(params.slug)(dispatch, getState),
            ])
        },
    },
    {
        name: 'recipes',
        path: '/recipes',
        onActivate: () => function (dispatch, getState) {
            return Promise.all([
                fetchCategories()(dispatch, getState),  // TODO: Crutch: we need fetch categories once on Server side
                fetchRecipesList()(dispatch, getState),
            ])
        },
    },
    {
        name: 'recipes.single',
        path: '/:slug',
        onActivate: (params) => function (dispatch, getState) {
            return Promise.all([
                fetchCategories()(dispatch, getState),  // TODO: Crutch: we need fetch categories once on Server side
                fetchRecipeBySlug(params.slug)(dispatch, getState),
            ])
        },
    },
    {
        name: 'contacts',
        path: '/trade_places',
        onActivate: () => function (dispatch, getState) {
            return Promise.all([
                fetchPlacesList()(dispatch, getState)
            ])
        },
    }
]
