import fs from 'fs';
import path from 'path';
import React from 'react';
import express from 'express';
import {renderToString} from 'react-dom/server';
import {HelmetProvider} from 'react-helmet-async';
import {Provider} from 'react-redux';
import serialize from 'serialize-javascript';
import {cloneRouter} from 'router5'
import {RouterProvider} from "react-router5";
import {I18nextProvider} from 'react-i18next'; // has no proper import yet
import Backend from 'i18next-node-fs-backend';
const i18nextMiddleware = require('i18next-express-middleware');

import i18n from './i18n';
import App from './containers/App';
import routes from "./routes";
import createRouter, {onRouteActivateMiddleware} from './createRouter'
import configureStore from './configureStore'

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebookincubator/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);
const appSrc = resolveApp('src');

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);


const serverRender = async (req, router, store) => {
    const context = {};
    const helmetContext = {};
    const markup = renderToString(
        <I18nextProvider i18n={req.i18n}>
            <Provider store={store}>
                <HelmetProvider context={helmetContext}>
                    <RouterProvider router={router}>
                        <App/>
                    </RouterProvider>
                </HelmetProvider>
            </Provider>
        </I18nextProvider>
    );

    // Grab the final data after rendering
    const {helmet} = helmetContext;
    const finalState = store.getState();

    return {markup, context, finalState, helmet}
};

const putIntoHtmlDocument = (req, markup, finalState, helmet, assets) => {
    const initialI18nStore = {};
    req.i18n.languages.forEach(l => {
        initialI18nStore[l] = req.i18n.services.resourceStore.data[l];
    });
    const initialLanguage = req.i18n.language;

    return `<!doctype html>
    <html ${helmet.htmlAttributes.toString()}>
    <head>
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${helmet.script.toString()}
        
        ${
        assets.client.css
            ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ''
        }
        ${
        process.env.NODE_ENV === 'production'
            ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
        }
		<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    </head>
    <body ${helmet.bodyAttributes.toString()}>
        <div id="root">${markup}</div>
        <script>
            window.__PRELOADED_STATE__ = ${serialize(finalState)};
            window.__ROUTER_STATE__ = ${serialize(finalState.router.route)};
            window.initialI18nStore = ${serialize(initialI18nStore)};
            window.initialLanguage = '${initialLanguage}';
        </script>
    </body>
</html>`
};

const server = express();
const baseRouter = createRouter();

i18n
    .use(Backend)
    .use(i18nextMiddleware.LanguageDetector)
    .init(
        {
            debug: false,
            preload: ['ru'],
            ns: ['translations'],
            defaultNS: 'translations',
            backend: {
                loadPath: `${appSrc}/locales/{{lng}}/{{ns}}.json`,
                addPath: `${appSrc}/locales/{{lng}}/{{ns}}.missing.json`,
            },
        },
        () => {
            server
                .disable('x-powered-by')
                .use(i18nextMiddleware.handle(i18n))
                .use('/locales', express.static(`${appSrc}/locales`))
                .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
                .get('/*', async (req, res) => {
                    req.i18n.changeLanguage('ru');

                    const router = cloneRouter(baseRouter);
                    const preloadedState = {
                        router: {
                            route: router.getState(),
                            previousRoute: null,
                            transitionRoute: null,
                            transitionError: null,
                        },
                    };
                    const store = configureStore(preloadedState, router, true);
                    router.setDependency('store', store);
                    router.useMiddleware(onRouteActivateMiddleware(routes));

                    await router.start( req.originalUrl, async function done(error, state) {
                        if ( error ) {
                            res.status( 500 ).send( error );
                        } else {
                            const {markup, context, finalState, helmet} = await serverRender(req, router, store);

                            if (context.url) {
                                res.redirect(context.url);
                            } else {
                                res.status(200).send(
                                    putIntoHtmlDocument(req, markup, finalState, helmet, assets)
                                );
                            }
                        }
                    });
                });
        },
    );

export default server;
