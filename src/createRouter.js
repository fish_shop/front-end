import createRouter from 'router5'
import loggerPlugin from 'router5-plugin-logger'
import browserPlugin from 'router5-plugin-browser'
import transitionPath from 'router5-transition-path';

import routes from './routes'


export const onRouteActivateMiddleware = (routes) => (router, dependencies) => (toState, fromState) => {
    console.log('We are in middleware');

    const {toActivate} = transitionPath(toState, fromState);
    const onActivateHandlers =
        toActivate
            .map(segment => routes.find(r => r.name === segment).onActivate)
            .filter(Boolean);
    return Promise
        .all(onActivateHandlers.map(callback => callback(toState.params)(
            dependencies.store.dispatch,
            dependencies.store.getState,
        )))
        .then(_ => toState);
};

export default function configureRouter() {
    const router = createRouter(
        routes,
        {
            trailingSlash: true,
            defaultRoute: '404'
        }
    );

    // Plugins
    router.usePlugin(browserPlugin());
    router.usePlugin(loggerPlugin);

    return router;
}
