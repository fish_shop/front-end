import {getRequest} from "./requests";


export const placesListRequest = () => {
    return getRequest('/trade_places');
};