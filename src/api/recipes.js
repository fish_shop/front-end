import {getRequest} from "./requests";


export const recipesListRequest = (limit, offset) => {
    return getRequest('/recipes', {limit: limit, offset: offset});
};

export const recipeBySlugRequest = (slug) => {
    return getRequest(`/recipes/${slug}`);
};
