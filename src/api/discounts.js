import {getRequest} from "./requests";


export const discountListRequest = (limit, offset) => {
    return getRequest('/discounts', {limit: limit, offset: offset});
};

export const discountBySlugRequest = (slug) => {
    return getRequest(`/discounts/${slug}`);
};
