import axios from "axios";


const baseUrl = process.env.REACT_APP_API_HOST ? process.env.REACT_APP_API_HOST : 'https://dev.seafoods-group.ru';
const instance = axios.create({
    baseURL: baseUrl,
    timeout: 3000,
});

export const getRequest = (url, params={}) => {
    return instance.get("/api" + url + '/', {params: params})
};

export const postRequest = (url, params={}) => {
    return instance.post("/api" + url + '/', {params: params})
};
