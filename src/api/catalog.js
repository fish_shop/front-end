import {getRequest, postRequest} from "./requests";


export const categoriesRequest = () => {
    return getRequest('/categories');
};

export const productsRequest = (params={}) => {
    return getRequest('/products', params);
};

export const filtersRequest = () => {
    return postRequest('/filters');
};