import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk';
import {router5Middleware} from 'redux-router5'
import {composeWithDevTools} from 'redux-devtools-extension';

import rootReducer from './reducers';


const configureStore = (preloadedState, router, isServer) => {
    const middlewares = [
        thunk,
        router5Middleware(router),
    ];

    if (!isServer) {
        middlewares.push(
            createLogger({
                diff: true
            })
        );
    }

    const store = createStore(
        rootReducer,
        preloadedState,
        composeWithDevTools(
            applyMiddleware(...middlewares),
        )
    );

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('./reducers', () => {
            const nextRootReducer = require('./reducers').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
};

export default configureStore;
