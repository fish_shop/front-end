export const RECIPES_LIST_REQUEST_SUCCESS = 'RECIPES/RECIPES_LIST_REQUEST_SUCCESS';
export const RECIPES_LIST_REQUEST_FAIL = 'RECIPES/RECIPES_LIST_REQUEST_FAIL';

export const RECIPE_BY_SLUG_REQUEST_SUCCESS = 'DISCOUNTS/RECIPE_BY_SLUG_REQUEST_SUCCESS';
export const RECIPE_BY_SLUG_REQUEST_FAIL = 'DISCOUNTS/RECIPE_BY_SLUG_REQUEST_FAIL';


export const recipesListRequestSuccess = (items) => ({
    type: RECIPES_LIST_REQUEST_SUCCESS,
    items,
});

export const recipesListRequestFail = (error) => ({
    type: RECIPES_LIST_REQUEST_FAIL,
    error,
});

export const recipeBySlugRequestSuccess = (data) => ({
    type: RECIPE_BY_SLUG_REQUEST_SUCCESS,
    data
});

export const recipeBySlugRequestFail = (error) => ({
    type: RECIPE_BY_SLUG_REQUEST_FAIL,
    error,
});
