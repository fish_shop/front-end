export const DISCOUNT_LIST_REQUEST_SUCCESS = 'DISCOUNTS/DISCOUNT_LIST_REQUEST_SUCCESS';
export const DISCOUNT_LIST_REQUEST_FAIL = 'DISCOUNTS/DISCOUNT_LIST_REQUEST_FAIL';

export const DISCOUNT_BY_SLUG_REQUEST_SUCCESS = 'DISCOUNTS/DISCOUNT_BY_SLUG_REQUEST_SUCCESS';
export const DISCOUNT_BY_SLUG_REQUEST_FAIL = 'DISCOUNTS/DISCOUNT_BY_SLUG_REQUEST_FAIL';


export const discountListRequestSuccess = (items) => ({
    type: DISCOUNT_LIST_REQUEST_SUCCESS,
    items,
});

export const discountListRequestFail = (error) => ({
    type: DISCOUNT_LIST_REQUEST_FAIL,
    error,
});

export const discountBySlugRequestSuccess = (data) => ({
    type: DISCOUNT_BY_SLUG_REQUEST_SUCCESS,
    data
});

export const discountBySlugRequestFail = (error) => ({
    type: DISCOUNT_BY_SLUG_REQUEST_FAIL,
    error,
});
