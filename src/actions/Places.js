export const PLACES_LIST_REQUEST_SUCCESS = 'PLACES/PLACES_LIST_REQUEST_SUCCESS'
export const PLACES_LIST_REQUEST_FAIL = 'PLACES/PLACES_LIST_REQUEST_FAIL'



export const placesListRequestSuccess = (items) => ({
    type: PLACES_LIST_REQUEST_SUCCESS,
    items,
});

export const placesListRequestFail = (error) => ({
    type: PLACES_LIST_REQUEST_FAIL,
    error,
});