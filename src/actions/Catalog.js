export const CATEGORIES_REQUEST_SUCCESS = 'CATALOG/CATEGORIES_REQUEST_SUCCESS';
export const CATEGORIES_REQUEST_FAIL = 'CATALOG/CATEGORIES_REQUEST_FAIL';

export const PRODUCTS_REQUEST_START = 'CATALOG/PRODUCTS_REQUEST_START';
export const PRODUCTS_REQUEST_SUCCESS = 'CATALOG/PRODUCTS_REQUEST_SUCCESS';
export const PRODUCTS_REQUEST_FAIL = 'CATALOG/PRODUCTS_REQUEST_FAIL';

export const FILTERS_REQUEST_SUCCESS = 'CATALOG/FILTERS_REQUEST_SUCCESS';
export const FILTERS_REQUEST_FAIL = 'CATALOG/FILTERS_REQUEST_FAIL';


export const categoriesRequestSuccess = (categories) => ({
    type: CATEGORIES_REQUEST_SUCCESS,
    categories,
});

export const categoriesRequestFail = (error) => ({
    type: CATEGORIES_REQUEST_FAIL,
    error,
});

export const productsRequestStart = (products) => ({
    type: PRODUCTS_REQUEST_START,
    products,
});

export const productsRequestSuccess = (products) => ({
    type: PRODUCTS_REQUEST_SUCCESS,
    products,
});

export const productsRequestFail = (error) => ({
    type: PRODUCTS_REQUEST_FAIL,
    error,
});

export const filtersRequestSuccess = (filters) => ({
    type: FILTERS_REQUEST_SUCCESS,
    filters,
});

export const filtersRequestFail = (error) => ({
    type: FILTERS_REQUEST_FAIL,
    error,
});
