# SSR catalog front-end boilerplate

## TODOs:
* [x]  **Task #5:** Avoid prefetch categories (Public Layout as root url component)
* [x]  **Task #5:** Preload page data before route to it.
* [ ]  Add critical CSS to page before show it first time.
* [ ]  Draw preloading blue line

#### На будущее:
* [ ]  ЧПУ (url replace without pushing to history)
* [ ]  Decode & encode url-параметров
* [ ]  Realtime props type checking using [prop-types](https://www.npmjs.com/package/prop-types).

## Ideas behind the scenes
Most of the ideas comes fron [here](https://github.com/brillout/awesome-universal-rendering).

#### Prefetch components data
Для предзагрузки данных на сервере используем [React frontload](https://github.com/davnicwil/react-frontload). Special thanks to [this guide](https://medium.com/@cereallarceny/server-side-rendering-in-create-react-app-with-all-the-goodies-without-ejecting-4c889d7db25e).  
На клиенте - [Router 5 middleware](https://router5.js.org/advanced/loading-async-data), причем мы ожидаем полной предзагрузки данных для новой страницы перед ее открытием.


- Данные, нужные для всего приложения, загружаются единоразово на сервере: `src/containers/App.js:15`
- Далее, данные, необходимые для отрисовки одной конкретной страницы, сейчас необходимо прописывать в 2х местах:
  1. Для предзагрузки на сервере при помощи frontload: `src/pages/Catalog.js:9`
  2. Для предзагрузки на клиенте перед переходом на эту страницу: `src/routes.js:12`
  > Это довольно тупо, т.к. мы смешиваем подходы загрузки данных исходя из компонент и загрузки данных исходя из роутов  


#### Router events in Redux
Используем [Router5 logging plugin](https://router5.js.org/advanced/plugins), который работает и на клиенте, и на сервере.  
Подключен плагин тут: `src/createRouter.js:35`

#### Dynamic meta-tags
Используем [React Helmet Async](https://github.com/staylor/react-helmet-async/issues) according to [this tutorial](https://open.nytimes.com/the-future-of-meta-tag-management-for-modern-react-development-ec26a7dc9183).

#### Translation
- Используем [i18next](https://github.com/i18next/i18next).
- Кешируем результат выбора языка в куку: `src/i18n.js:29`
