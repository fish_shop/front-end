'use strict';
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');


module.exports = {
    plugins: [
        'scss',
    ],
    modify(config, {target, dev}, webpack) {
        const appConfig = config; // stay immutable here

        config.plugins.push(new FriendlyErrorsWebpackPlugin());

        return appConfig;
    },
};
